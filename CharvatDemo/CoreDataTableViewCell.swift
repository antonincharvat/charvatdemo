//
//  CoreDataTableViewCell.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 20/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class CoreDataTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
