//
//  UkazkyCoreDataViewController.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 20/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import CoreData

class UkazkyCoreDataViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var stage = 0

    
    let coreDataProvider = CoreDataProvider.sharedInstance
    var fetchedResultsController: NSFetchedResultsController?
    
    var arrowImageView: UIImageView?
    
    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        setUpController()
        performOrientationAdjustment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        coordinator.animateAlongsideTransition({ [unowned self]
            context in
            self.performOrientationAdjustment()
            }, completion: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        arrowImageView?.removeFromSuperview()
        arrowImageView = nil
    }
    
    // MARK: Initial setup
    func setUpController() {
        // add bar button +
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(self.addPressed(_:)))
        navigationItem.rightBarButtonItem = addButton
        
        // init fetchedResultsController
        fetchedResultsController = coreDataProvider.createFetchedResultsControllerForEntity("DemoEntity")
        fetchedResultsController!.delegate = self
        
        // add an arrow
        let arrowImage = UIImage(named: "arrow_red")
        arrowImageView = UIImageView(image: arrowImage)
        navigationController?.view.addSubview(arrowImageView!)
        animateArrow()
    }
    
    func setUpCell(cell: CoreDataTableViewCell, atIndexPath indexPath: NSIndexPath) {
        let fetchedItem = fetchedResultsController!.objectAtIndexPath(indexPath)
        if let item = fetchedItem.valueForKey("item") {
            cell.label?.text = item as? String
        }
    }
    
    func performOrientationAdjustment() {
        if self.view.frame.height > self.view.frame.width {
            arrowImageView!.frame = CGRect(x: self.view.frame.size.width - 100, y: 15, width: 50, height: 50)
        }else{
            arrowImageView!.frame = CGRect(x: self.view.frame.size.width - 80, y: 0, width: 30, height: 30)
        }
    }
    
    // MARK: Controls
    func addPressed(sender: UIBarButtonItem) {
        // alert controller with textField preventing clicking OK without any text written
        let ac = UIAlertController(title: Texts.ukazkyCoreDataAlert[0], message: Texts.ukazkyCoreDataAlert[1], preferredStyle: .Alert)
        ac.addTextFieldWithConfigurationHandler { (textField) in
            if self.stage < Texts.ukazkyCoreDataTexts.count {
                textField.addTarget(self, action: #selector(self.textChanged(_:)), forControlEvents: .EditingChanged)
                textField.text = Texts.ukazkyCoreDataTexts[self.stage]
            }
        }
        ac.addAction(UIAlertAction(title: Texts.ok, style: .Default, handler: { (action) in
            if let text = ac.textFields![0].text {
                if text != "" {
                    self.coreDataProvider.saveItem(text, withKey: "item", forEntityForName: "DemoEntity")
                    self.stage += 1
                }else{
                    // TODO:
                    print("Zadejte text")
                }
            }
        }))
        ac.addAction(UIAlertAction(title: Texts.zpet, style: .Cancel, handler: nil))
        dispatch_async(dispatch_get_main_queue(), { [unowned self] in
            self.presentViewController(ac, animated: true, completion: nil)
        })
    }
    
    // MARK: Alert TextField Action
    func textChanged(textField: UITextField) {
        var responder: UIResponder = textField
        while !(responder is UIAlertController) {
            responder = responder.nextResponder()!
        }
        let ac = responder as! UIAlertController
        ac.actions[0].enabled = (textField.text != "")
    }
    
    // MARK: Animations
    func animateArrow() {
        if arrowImageView != nil {
            UIView.animateWithDuration(0.3, animations: {
                self.arrowImageView!.transform = CGAffineTransformMakeScale(0.95, 0.95)
                }, completion: { Void in
                    if self.arrowImageView != nil {
                        UIView.animateWithDuration(0.3, animations: {
                            self.arrowImageView!.transform = CGAffineTransformMakeScale(1.0, 1.0)
                            }, completion: { Void in
                                self.animateArrow()
                            }
                        )
                    }
                }
            )
        }
    }
}

// MARK: NSFetchedResultsControllerDelegate
extension UkazkyCoreDataViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch (type) {
        case .Insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break
        case .Delete:
            if let indexPath = indexPath {
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break
        case .Update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRowAtIndexPath(indexPath) as! CoreDataTableViewCell
                // TODO
            }
            break
        default:
            break
        }
    }
}

// MARK: UITableViewDataSource
extension UkazkyCoreDataViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let sections = fetchedResultsController!.sections {
            return sections.count
        }
        return 0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController!.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CoreDataCell", forIndexPath: indexPath) as! CoreDataTableViewCell
        setUpCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            if let record = fetchedResultsController?.objectAtIndexPath(indexPath) as? NSManagedObject {
                coreDataProvider.deleteObject(record)
            }
        }
    }
}

// MARK: UITableViewDelegate
extension UkazkyCoreDataViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // TODO: records editing
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
