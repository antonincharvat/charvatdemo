//
//  Texts.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 02/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import Foundation

struct Texts {
    // !!! NECOMMITOVAT !!!
    static let title = "Antonín Charvát Demo"
    static let sidebarMenuTexts = ["Co nabízím", "Co umím", "Ukázky", "O mně"]
    static let coNabizimTexts = ["Vývoj aplikací pro iOS v jazyce Swift", "Spolupráci na ŽL či HPP", "Dobře čitelný a přehledný kód", "1+ let profesionální praxe"]
    static let coUmimTexts = ["XCode + Swift", "UIKit", "CoreData", "Storyboard + AutoLayout", "Verzování s BitBucket + SourceTree", "Cocoapods", "Alamofire, Firebase", "...učit se nové věci"]
    static let ukazkyTexts = ["Práce s CoreData", "AutoLayout + Constraints", "HTTP Networking (Alamofire)", "Datové úložiště (Firebase)", "Parsování JSONu (Alamofire)"]
    static let omneTexts = ["Mgr. Antonín Charvát", "Narozen 28. září 1986", "Momentálně bytem Choceradská, Praha 4", "Tel.: 00420 734 793 228", "Napište mi email"]
    static let praxeTexts = ["Jen 1+ let zkušeností?", "Ano, ale jedná se o full time praxi 8 až 12 hodin denně, s primární orientací na Swift.", "To dává smysl"]
    
    static let emptyTextFieldError = ["Chyba", "Textové pole nemůže být prázdné"]
    
    static let ok = "OK"
    static let zpet = "Zpět"
    
    static let ukazkyCoreDataAlert = ["Zadejte text", "Tento text bude uložen pomocí CoreData frameworku"]
    static let ukazkyCoreDataTexts = ["Pro práci s CoreData jsem vytvořil singleton CoreDataProvider, ve kterém se fetch requestem naplní NSFetchedResultsController daty.", "Díky tomu je práce s daty v rámci malého projektu velmi snadná. K hodnotám je přistupováno pomocí key-value coding, které je pro malý projekt dostačující.", "Pro velký projekt je nutné vytvořit objekty které budou dědit z NSManagedObject."]
    
    static let ukazkyAutoLayoutTexts = ["Nakloňte zařízení do režimu Landscape", "Všechny prvky se přizpůsobí zvolenému zobrazení díky constraints"]
    
    static let ukazkyHttpsButtonText = "Stáhnout"
    static let ukazkyHttpsTextFieldText = "Stiskněte Stáhnout pro stažení dat"
    static let ukazkyHttpsTextFieldTextDone = "Přenos proběhl v pořádku"
    
    static let ukazkyDataStorageTextFieldText = "Text k uložení do online databáze"
    static let ukazkyDataStorageInfoLabelText = "Swipe přes řádek v tabulce zobrazí další možnosti"
    static let ukazkyDataStorageButtonText = "Uložit"
    static let ukazkyDataStorageEditButton = "Upravit"
    static let ukazkyDataStorageEditTitle = "Upravte záznam"
    static let ukazkyDataStorageEditSave = "Uložit"
    static let ukazkyDataStorageEditCancel = "Zrušit"
    
    static let ukazkyDeleteButton = "Smazat"
    
    static let ukazkyJSONButtonText = "Stáhnout"
    static let ukazkyJSONTextFieldText = "Stiskněte Stáhnout pro stažení dat"
    static let ukazkyJSONTextFieldTextDone = "Přenos proběhl v pořádku"
    
    static let sendmailURL = "http://sendmail.antonincharvat.cz"
    static let plistURL = "http://data.antonincharvat.cz/temp/charvat_data.plist"
    static let jsonURL = "http://data.antonincharvat.cz/temp/charvat_data.json"
    static let rootRef = "https://charvatdemo.firebaseio.com/"
}




