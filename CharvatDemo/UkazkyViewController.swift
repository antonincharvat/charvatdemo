//
//  UkazkyViewController.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 03/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class UkazkyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackView1trailing: NSLayoutConstraint!
    @IBOutlet weak var stackView1top: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    @IBOutlet weak var tableViewLeading: NSLayoutConstraint!
    
    let coreDataProvider = CoreDataProvider()

    var items: [AnyObject]?

    var itemsTuple: [(String, AnyObject)]? {
        didSet {
            if itemsTuple!.isEmpty && mode == .DataStorage {
                infoLabel.hidden = true
            }else if !itemsTuple!.isEmpty && mode == .DataStorage {
                infoLabel.hidden = false
            }
        }
    }
    
    enum Mode {
        case AutoLayout
        case Https
        case DataStorage
        case JSON
    }
    var mode: Mode?
    
    let rootRef = Firebase(url: Texts.rootRef)
    
    

    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        performOrientationAdjustment()
        setUp()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        coordinator.animateAlongsideTransition({ [unowned self]
            context in
            self.performOrientationAdjustment()
        }, completion: nil)
    }
    
    
    
    // MARK: Initial setup
    func setUp() {
        switch (mode!) {
        case .AutoLayout:
            textField1.enabled = false
            infoLabel.hidden = true
            button1.enabled = false
            items = Texts.ukazkyAutoLayoutTexts
            
        case .Https:
            textField1.text = Texts.ukazkyHttpsTextFieldText
            textField1.enabled = false
            infoLabel.hidden = true
            button1.setTitle(Texts.ukazkyHttpsButtonText, forState: .Normal)
            items = []
            
        case .DataStorage:
            textField1.becomeFirstResponder()
            textField1.text = Texts.ukazkyDataStorageTextFieldText
            infoLabel.text = Texts.ukazkyDataStorageInfoLabelText
            dispatch_async(dispatch_get_main_queue(), { [unowned self] in
                self.textField1.selectedTextRange = self.textField1.textRangeFromPosition(self.textField1.beginningOfDocument, toPosition: self.textField1.endOfDocument)
            })
            button1.setTitle(Texts.ukazkyDataStorageButtonText, forState: .Normal)
            itemsTuple = [(String, AnyObject)]()
            retrieveDataFromDB()
            
        case .JSON:
            textField1.text = Texts.ukazkyJSONTextFieldText
            textField1.enabled = false
            infoLabel.hidden = true
            button1.setTitle(Texts.ukazkyJSONButtonText, forState: .Normal)
            items = []
        }
    }
    
    func performOrientationAdjustment() {
        if self.view.frame.height > self.view.frame.width {
            self.stackView1trailing.constant = 0
            self.stackView1top.constant = 30
            self.tableViewLeading.constant = -20
            self.tableViewTop.constant = 88
        }else{
            self.stackView1trailing.constant = self.view.frame.size.width / 2
            self.stackView1top.constant = self.view.frame.size.height / 3
            self.tableViewLeading.constant = self.view.frame.size.width / 2
            self.tableViewTop.constant = 0
        }
        self.stackView.setNeedsLayout()
        self.stackView.layoutIfNeeded()
    }
    
    
    
    // MARK: Alamofire
    func fetchDataFromHttp() {
        Alamofire.request(.GET,
                          Texts.plistURL,
                          parameters: nil,
                          encoding: .PropertyList(.XMLFormat_v1_0, 0),
                          headers: nil).responsePropertyList { (response) in
                            
                            if let error = response.result.error {
                                print("Error while fetching data: \(error)")
                            }else if let data = response.result.value as? [String: String] {
                                if data.isEmpty {
                                    print("No data fetched")
                                }else{
                                    for item in data {
                                        self.items?.append(item.1)
                                    }
                                    self.items = self.items?.reverse()
                                    
                                    self.textField1.text = Texts.ukazkyHttpsTextFieldTextDone
                                    
                                    var indexPaths = [NSIndexPath]()
                                    
                                    for item in self.items! {
                                        let indexPath = NSIndexPath.init(forRow: self.items!.indexOf{$0 === item}!, inSection: 0)
                                        indexPaths.append(indexPath)
                                    }
                                    
                                    self.tableView.beginUpdates()
                                        self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Right)
                                    self.tableView.endUpdates()
                                }
                            }
        }
    }
    
    func fetchDataFromHttpJSON() {
        Alamofire.request(.GET,
            Texts.jsonURL,
            parameters: nil,
            encoding: .JSON,
            headers: nil).responseJSON { (response) in
                if let error = response.result.error {
                    print("Error while fetching data: \(error)")
                }else if let data = response.result.value as? NSMutableArray{
                    if data == [] {
                        print("No data fetched")
                    }else{
                        if let items = data[0] as? [String: String] {
                            var itemKey = "item0"
                            for index in 1.stride(to: items.count, by: 1){
                                itemKey = String(itemKey.characters.dropLast())
                                itemKey += "\(index)"
                                let item = items[itemKey]
                                self.items?.append(item!)
                            }
                        }
                        
                        self.textField1.text = Texts.ukazkyJSONTextFieldTextDone
                        
                        var indexPaths = [NSIndexPath]()
                        
                        for item in self.items! {
                            let indexPath = NSIndexPath.init(forRow: self.items!.indexOf{$0 === item}!, inSection: 0)
                            indexPaths.append(indexPath)
                        }
                        
                        self.tableView.beginUpdates()
                            self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Right)
                        self.tableView.endUpdates()
                    }
                }
        }
    }
    
    
    
    // MARK: Firebase
    func saveDataToDB(data: String) {
        let demoRef = rootRef.childByAutoId()
        demoRef.setValue(data)
    }
    
    func retrieveDataFromDB() {
        self.rootRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if snapshot.value is NSNull {
                print("No data")
            }else{
                for item in snapshot.children.allObjects as! [FDataSnapshot] {
                    self.itemsTuple!.append((item.key, item.value))
                }
                                
                var indexPaths = [NSIndexPath]()
                
                for item in self.itemsTuple! {
                    let indexPath = NSIndexPath.init(forRow: self.itemsTuple!.indexOf{$0.0 == item.0}!, inSection: 0)
                    indexPaths.append(indexPath)
                }

                self.tableView.beginUpdates()
                    self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Right)
                self.tableView.endUpdates()
                
                self.rootRef.removeAllObservers()
            }
        })
    }
    
    func retrieveDataAndUpdateTableView() {
        self.rootRef.observeSingleEventOfType(.ChildAdded, withBlock: { snapshot in
            if snapshot.value is NSNull {
                print("No data")
            }else{
                self.itemsTuple!.append((snapshot.key, snapshot.value))
                
                self.tableView.beginUpdates()
                    self.tableView.insertRowsAtIndexPaths([NSIndexPath.init(forRow: self.itemsTuple!.count - 1, inSection: 0)], withRowAnimation: .Right)
                self.tableView.endUpdates()
                
                self.rootRef.removeAllObservers()
            }
        })
    }
    
    func retrieveDataAndUpdateRowAtIndexPath(indexPath: NSIndexPath) {
        self.rootRef.observeSingleEventOfType(.ChildAdded, withBlock: { snapshot in
            if snapshot.value is NSNull {
                print("No data")
            }else{
                self.itemsTuple!.removeAtIndex(indexPath.row)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.itemsTuple!.insert((snapshot.key, snapshot.value), atIndex: indexPath.row)
                
                self.tableView.beginUpdates()
                    self.tableView.insertRowsAtIndexPaths([NSIndexPath.init(forRow: indexPath.row, inSection: 0)], withRowAnimation: .Right)
                self.tableView.endUpdates()
                
                self.rootRef.removeAllObservers()
            }
        })
    }
    
    func removeObjectFromDBWithKey(key: String) {
        let childRef = rootRef.childByAppendingPath(key)
        childRef.removeValue()
        
        let removeIndex = self.itemsTuple!.indexOf({$0.0 == key})
        let indexPath = NSIndexPath.init(forRow: removeIndex!, inSection: 0)
        self.itemsTuple?.removeAtIndex(removeIndex!)
        
        self.tableView.beginUpdates()
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        self.tableView.endUpdates()
    }
    
    func updateObjectInDBWithKey(key: String, withValue value: AnyObject) {
        let childRef = rootRef.childByAppendingPath(key)
        childRef.setValue(value)

    }
    
    
    // MARK: Helpers
    func presentAlertWithTitle(title: String, message: String) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        ac.addAction(UIAlertAction(title: Texts.ok, style: .Cancel, handler: nil))
        self.presentViewController(ac, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: UITableViewDataSource
extension UkazkyViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mode == .DataStorage {
            return itemsTuple!.count
        }
        return items!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UkazkyCell") as? UkazkyTableViewCell
        
        if mode == .DataStorage {
            cell?.label.text = itemsTuple![indexPath.row].1 as? String
        }else{
            cell?.label.text = items![indexPath.row] as? String
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if mode == .DataStorage {
            return true
        }
        return false
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        var actions: [UITableViewRowAction] = []
        if mode == .DataStorage {
            let editAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: Texts.ukazkyDataStorageEditButton) { (action, indexPath) in
                let ac = UIAlertController(title: Texts.ukazkyDataStorageEditTitle, message: nil, preferredStyle: .Alert)
                ac.addTextFieldWithConfigurationHandler({ (textField) in
                    textField.becomeFirstResponder()
                    textField.text = self.itemsTuple![indexPath.row].1 as? String
                })
                ac.addAction(UIAlertAction(title: Texts.ukazkyDataStorageEditSave, style: .Default, handler: { (action) in
                    self.updateObjectInDBWithKey(self.itemsTuple![indexPath.row].0, withValue: ac.textFields![0].text!)
                    self.retrieveDataAndUpdateRowAtIndexPath(indexPath)
                }))
                ac.addAction(UIAlertAction(title: Texts.ukazkyDataStorageEditCancel, style: .Cancel, handler: nil))
                self.presentViewController(ac, animated: true, completion: nil)
            }
            editAction.backgroundColor = UIColor.init(red: 210/255, green: 225/255, blue: 255/255, alpha: 1.0)
            actions.append(editAction)
        }
        
        let deleteAction = UITableViewRowAction(style: .Normal, title: Texts.ukazkyDeleteButton) { (action, indexPath) in
            if self.mode == .DataStorage {
                self.removeObjectFromDBWithKey(self.itemsTuple![indexPath.row].0)
            }
        }
        deleteAction.backgroundColor = UIColor.init(red: 255/255, green: 153/255, blue: 116/255, alpha: 1.0)
        actions.append(deleteAction)
        
        return actions
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {

        }
    }
}

// MARK: UITableViewDelegate
extension UkazkyViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: Controls
extension UkazkyViewController {
    @IBAction func button1pressed(sender: UIButton) {
        switch (mode!) {
        case .Https:
            button1.enabled = false
            fetchDataFromHttp()
            
        case .DataStorage:
            if let itemToSave = textField1.text {
                if itemToSave != "" {
                    saveDataToDB(itemToSave)
                    retrieveDataAndUpdateTableView()
                }else{
                    self.presentAlertWithTitle(Texts.emptyTextFieldError[0], message: Texts.emptyTextFieldError[1])
                }
            }
            
        case .JSON:
            button1.enabled = false
            fetchDataFromHttpJSON()
            
        default:
            break
        }
    }
}

