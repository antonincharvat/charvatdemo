//
//  CoreDataProvider.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 03/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import CoreData

class CoreDataProvider: NSObject {
    class var sharedInstance: CoreDataProvider {
        struct Static {
            static let instance: CoreDataProvider = CoreDataProvider()
        }
        return Static.instance
    }
    
    var appDel: AppDelegate
    var context: NSManagedObjectContext
    var fetchedItems: [NSManagedObject]?
    
    override init() {
        self.appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        self.context = appDel.managedObjectContext
    }

    func saveItem(item: AnyObject, withKey key: String, forEntityForName entity: String) {
        let newRecord = NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: self.context)
        newRecord.setValue(item, forKey: key)
        newRecord.setValue(NSDate(), forKey: "created")
        
        do {
            try context.save()
        } catch {
            print("Error while saving into CoreData: \(error)")
        }
    }
    
    func deleteObject(object: NSManagedObject) {
        context.deleteObject(object)
        
        do {
            try context.save()
        } catch {
            print("Error while deleting from CoreData: \(error)")

        }
    }
    
    func createFetchedResultsControllerForEntity(entity: String) -> NSFetchedResultsController {
        let fetchRequest = NSFetchRequest(entityName: entity)
        let sortDescriptor = NSSortDescriptor(key: "created", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        do {
            try frc.performFetch()
        } catch {
            print(error)
        }
        
        return frc
    }
}
