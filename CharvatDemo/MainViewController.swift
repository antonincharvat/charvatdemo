//
//  MainViewController.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 02/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit
import SafariServices

class MainViewController: UIViewController, SidebarMenuDelegate {

    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    enum Mode{
        case CoNabizim
        case CoUmim
        case Ukazky
        case Omne
        case Default
    }
    var mode: Mode = .Default
    
    
    
    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.hidden = true
        
        self.title = Texts.title
    }
    
    override func viewDidAppear(animated: Bool) {
        animateArrow()
    }
    
    
    
    // MARK: SidebarMenu
    @IBAction func showSidebarMenu(sender: UIBarButtonItem) {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("SidebarMenuViewController") as? SidebarMenuViewController
        vc?.modalPresentationStyle = .OverCurrentContext
        vc?.delegate = self
        self.presentViewController(vc!, animated: false, completion: nil)
    }
    
    func sidebarMenuItemSelected(item: Int) {
        if arrowImage != nil {
            arrowImage.removeFromSuperview()
            arrowImage = nil
            tableView.hidden = false
        }
        if item == 0 {
            mode = Mode.CoNabizim
            tableView.allowsSelection = false
        }else if item == 1 {
            mode = Mode.CoUmim
            tableView.allowsSelection = false
        }else if item == 2 {
            mode = Mode.Ukazky
            tableView.allowsSelection = true
        }else if item == 3 {
            mode = Mode.Omne
            tableView.allowsSelection = true
        }
        self.title = Texts.sidebarMenuTexts[item]
        tableView.reloadData()
    }

    
    
    // MARK: Animations
    func animateArrow() {
        if arrowImage != nil {
            UIView.animateWithDuration(0.3, animations: {
                self.arrowImage.transform = CGAffineTransformMakeScale(0.95, 0.95)
                }, completion: { Void in
                    if self.arrowImage != nil {
                        UIView.animateWithDuration(0.3, animations: {
                            self.arrowImage.transform = CGAffineTransformMakeScale(1.0, 1.0)
                            }, completion: { Void in
                                self.animateArrow()
                            }
                        )
                    }
                }
            )
        }
    }
    

    
    // MARK: Helpers
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (mode) {
        case .CoNabizim:
            return Texts.coNabizimTexts.count
        case .CoUmim:
            return Texts.coUmimTexts.count
        case .Ukazky:
            return Texts.ukazkyTexts.count
        case .Omne:
            return Texts.omneTexts.count
        case .Default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        
        switch (mode) {
        case .CoNabizim:
            cell?.textLabel!.text = Texts.coNabizimTexts[indexPath.row]
            if indexPath.row == 3 {
                cell?.accessoryType = .DetailButton
            }else{
                cell?.accessoryType = .None
            }
        case .CoUmim:
            cell?.textLabel!.text = Texts.coUmimTexts[indexPath.row]
            cell?.accessoryType = .None
        case .Ukazky:
            cell?.textLabel!.text = Texts.ukazkyTexts[indexPath.row]
            cell?.accessoryType = .DisclosureIndicator
        case .Omne:
            cell?.textLabel!.text = Texts.omneTexts[indexPath.row]
            if indexPath.row == 4 {
                cell?.accessoryType = .DisclosureIndicator
            }else{
                cell?.accessoryType = .None
                cell?.selectionStyle = .None
            }
        case .Default:
            break
        }
        return cell!
        
    }
}

// MARK: UITableViewDelegate
extension MainViewController:  UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (mode) {
        case .Ukazky:
            let vc = storyboard?.instantiateViewControllerWithIdentifier("UkazkyViewController") as! UkazkyViewController
            
            if indexPath.row == 0 {
                //vc.mode = .CoreData
                //self.showViewController(vc, sender: self)

                let vc = storyboard?.instantiateViewControllerWithIdentifier("UkazkyCoreDataViewController") as! UkazkyCoreDataViewController
                self.showViewController(vc, sender: self)

            }else if indexPath.row == 1 {
                vc.mode = .AutoLayout
                self.showViewController(vc, sender: self)

            }else if indexPath.row == 2 {
                vc.mode = .Https
                self.showViewController(vc, sender: self)

            }else if indexPath.row == 3 {
                vc.mode = .DataStorage
                self.showViewController(vc, sender: self)

            }else if indexPath.row == 4 {
                vc.mode = .JSON
                self.showViewController(vc, sender: self)

            }
            
        case .Omne:
            if indexPath.row == 4 {
                let svc = SFSafariViewController(URL: NSURL(string: Texts.sendmailURL)!)
                self.presentViewController(svc, animated: true, completion: nil)
            }

        default:
            break
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        switch (mode) {
        case .CoNabizim:
            let ac = UIAlertController()
            ac.setValue(UIAlertControllerStyle.Alert.rawValue, forKey: "preferredStyle")
            ac.title = Texts.praxeTexts[0]
            ac.message = Texts.praxeTexts[1]
            ac.addAction(UIAlertAction(title: Texts.praxeTexts[2], style: .Cancel, handler: nil))
            self.presentViewController(ac, animated: true, completion: nil)
        default:
            break
        }
    }
}
