//
//  SidebarMenuTableViewCell.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 02/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class SidebarMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
