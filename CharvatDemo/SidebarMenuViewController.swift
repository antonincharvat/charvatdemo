//
//  SidebarMenuViewController.swift
//  CharvatDemo
//
//  Created by Antonín Charvát on 02/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

protocol SidebarMenuDelegate {
    func sidebarMenuItemSelected(item: Int)
}

class SidebarMenuViewController: UIViewController {

    @IBOutlet weak var sidebarMenuView: UIView!
    @IBOutlet weak var sidebarMenuViewLeading: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var tapRecognizer: UITapGestureRecognizer?
    var swipeRecognizer: UISwipeGestureRecognizer?
    
    var dimmedView: UIView?
    
    var delegate: SidebarMenuDelegate?
    
    
    
    // MARK: Startup
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
                
        self.view.backgroundColor = UIColor.clearColor()
        self.view.userInteractionEnabled = false
        
        sidebarMenuViewLeading.constant = -sidebarMenuView.frame.size.width
    }
    
    override func viewDidAppear(animated: Bool) {
        presentSidebarMenu(true)
    }


    
    // MARK: SidebarMenu
    func presentSidebarMenu(shouldOpen: Bool) {
        var constraint: CGFloat?
        var alpha: CGFloat?
        
        if shouldOpen {
            dimmedView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
            dimmedView?.backgroundColor = UIColor.clearColor()
            self.view.insertSubview(dimmedView!, atIndex: 0)
            self.view.userInteractionEnabled = true
            
            constraint = 0
            alpha = 0.6
        }else{
            constraint = -sidebarMenuView.frame.size.width
            alpha = 0.0
        }
        
        UIView.animateWithDuration(0.5, animations: { [unowned self] in
            self.sidebarMenuViewLeading.constant = constraint!
            self.dimmedView?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(alpha!)
            self.view.layoutIfNeeded()
            }, completion: { Void in
                if shouldOpen {
                    self.tapRecognizer = UITapGestureRecognizer()
                    self.tapRecognizer?.addTarget(self, action: #selector(self.handleTap(_:)))
                    self.dimmedView!.addGestureRecognizer(self.tapRecognizer!)
                    
                    self.swipeRecognizer = UISwipeGestureRecognizer()
                    self.swipeRecognizer?.addTarget(self, action: #selector(self.handleSwipe(_:)))
                    self.swipeRecognizer?.direction = .Left
                    self.dimmedView?.addGestureRecognizer(self.swipeRecognizer!)

                }else{
                    self.dimmedView?.removeGestureRecognizer(self.tapRecognizer!)
                    self.dimmedView?.removeFromSuperview()
                    self.view.userInteractionEnabled = false
                    self.dismissViewControllerAnimated(false, completion: nil)
                }
            })
    }

    
    
    // MARK: Gestures
    func handleTap(recognizer: UITapGestureRecognizer){
        presentSidebarMenu(false)
    }
    
    func handleSwipe(recognizer: UISwipeGestureRecognizer){
        presentSidebarMenu(false)
    }
    
    
    
    // MARK: Helpers
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// MARK: UITableViewDataSource
extension SidebarMenuViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Texts.sidebarMenuTexts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SidebarMenuCell") as! SidebarMenuTableViewCell
        cell.label.text = Texts.sidebarMenuTexts[indexPath.row]
        return cell
    }
}

// MARK: UITableViewDelegate
extension SidebarMenuViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate?.sidebarMenuItemSelected(indexPath.row)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        presentSidebarMenu(false)
    }
}
